import sys
import os
from hoomd import *
from hoomd import md
from hoomd import deprecated as hdepr
import numpy as np



#-------------------------HOOMD starts -------------------------------------------------
context.initialize()
# Place the type R central particles

snapshot = data.make_snapshot(N=18, particle_types=['A', 'B', 'R'],box=data.boxdim(L=100))
coord_center = [(0,0,-3), (0,0,3)]
for i in range(2):
    snapshot.particles.position[i] = coord_center[i]
    snapshot.particles.body[i] = i
    snapshot.particles.typeid[i] = 2
    snapshot.particles.mass[i] = 10
    snapshot.particles.moment_inertia = [[0,
                                               1/12*1.0*8**2,
                                               1/12*1.0*8**2]]
    snapshot.particles.orientation = [[1, 0, 0,0]]

system = init.read_snapshot(snapshot)

# Add consituent particles of type A and create the rods
#system.particles.types.add('A');

#system.particles.types.add('C');

rigid = md.constrain.rigid();
rigid.set_param('R',
                types=['A']*8,
                positions=[(-4,0,0),(-3,0,0),(-2,0,0),(-1,0,0),
                           (1,0,0),(2,0,0),(3,0,0),(4,0,0)]);

rigid.create_bodies()
xlm1 = hdepr.dump.xml(group=group.all(), filename='init.xml')
# Forcefield
sigA = 1.0
nl = md.nlist.cell()
lj = md.pair.lj(r_cut=3.5*sigA, nlist=nl)
lj.pair_coeff.set('A', 'A', epsilon=1.0, sigma=sigA)
lj.pair_coeff.set('A', 'B', epsilon=0, sigma=sigA)
lj.pair_coeff.set('B', 'B', epsilon=1.0, sigma=sigA)
lj.pair_coeff.set(['A', 'B', 'R'], 'R', epsilon=0, sigma=sigA)


rigid = group.rigid_center();
ALL = group.all()
md.integrate.mode_standard(dt=0.001)
#md.integrate.nve(group=rigid)
langv=md.integrate.langevin(group=rigid, kT=1.0, seed=42);


dump.gsd(filename='center.gsd',
         overwrite=True, period=500,
         group=group.all())

run(2e4)
