import sys
sys.path.append('/home/yrdahal/hoomd-install/')
sys.path.append('/home/yrdahal/hoomd-install/hoomd')
#sys.path.append('home/yrdahal/hoomd-test/lib/python')
#sys.path.append('/home/yrdahal/hoomd-test/lib/python')
sys.path.append('/projects/b1030/hoomd/hoomd-2.2.0_cuda8.0/')
sys.path.append('/projects/b1030/hoomd/hoomd-2.2.0_cuda8.0/hoomd/')
import os
import random
import hoomd
from hoomd import *
from hoomd.md import *
import hoomd.deprecated as deprecated 
import numpy as np 
c=context.initialize()
#buffer = option.get_user()
prot_r = 62.37
d_bb = 3.0
rad_M = d_bb/(2.0*2.0**(1./6))
rad_A = d_bb/(2.0*2.0**(1./6))
rad_N = 2.0
bead_N = 3
l_l = 20.0
#d_bb = 2.0*2.48*2.0**(1./6)
#l_l = 2.0*2.0**(1./6)*(rad_M + rad_N) + (bead_N - 3)*2.0**(1./6)*(rad_N + rad_N)
box_dims = 2.0/(3**0.5)*(2.0*prot_r+l_l+2.0*d_bb) #float(buffer[0])box_dims = 200
box_dimsx =box_dims
box_dimsy =box_dims
box_dimsz =box_dims
rad_B = 2.0#float(buffer[4])
rad_C = 2.0#float(buffer[5])
rad_R = 2.0 #float(buffer[6])
bead_position=[[l_l/2,0,0],[-l_l/2,0,0]]# two beads on either sides of central bead2
particles_list = np.genfromtxt("SBCG_ferritin_coordinate.dat",dtype =str)
linker_center = np.genfromtxt("coordinate_rigid_center_L10v2.dat",dtype =str)
prot_N = 2 #bcc template
N_A = 6.022 *(10) **23  # Avogadro's Constant reduced
molarity = 0.008 # in mole per litre
box_V = box_dims *box_dims *box_dims #volume of a unit cell in Angstrom unit
vol_p = 4.0/3 * (np.pi * prot_r **3) #volume of a protein
vol_P = prot_N * vol_p #total volume of a protein in a unit cell
vol = (box_V - vol_P)*(10**(-30)) #volume available for polymers in m^3 unit
num_poly = int(vol * molarity/(10**(-3)) * N_A) # number of polymers in a unit cell. it is divided by 10^-3 to convert liter into m^3
nbr_poly =5*prot_N; #number of polymer
print nbr_poly, num_poly
#quit()

linker_per_protein = 15
poly_nbr = prot_N * linker_per_protein #ratio of linker number to protein number is 20
linker0=[]
for p in linker_center:
    if len(linker0) < poly_nbr:
     linker0.append((float(p[0]), float(p[1]), float(p[2])))
#print len(linker0),linker0
#quit()    
total_ABC =len(particles_list)
lengthA=8
lengthB=0
lengthC=len(particles_list)-lengthA-lengthB
center_mass=65*lengthA+22*lengthB+50*lengthC
cage_diameter=62.37
massL = 196.0
lengthL = l_l
particle_type = ['R0']*prot_N + ['N']*poly_nbr
particle_position = [[0,0,0],[0.5 * box_dims,0.5* box_dims,0.5* box_dims]] + linker0
particle_mass = [center_mass]*prot_N + [massL]*poly_nbr
particle_diameter = [2*cage_diameter]*prot_N + [2*rad_N]*poly_nbr
particle_inertia = [[200.0/3*center_mass*cage_diameter**2,
                                               200.0/3*center_mass*cage_diameter**2,
                                               200.0/3*center_mass*cage_diameter**2]]*prot_N +[[200.0/12*massL*lengthL**2,
                                               200.0/12*massL*lengthL**2,
                                               200.0/12*massL*lengthL**2]]*poly_nbr
particle_orientation = [[1, 0, 0, 0]]+[[1, 0, 0, 0]]*(prot_N+poly_nbr-1)

uc = hoomd.lattice.unitcell(N = prot_N + poly_nbr,
                            a1 = [1.0*box_dimsx, 0,   0],
                            a2 = [0,    1.0*box_dimsy, 0],
                            a3 = [0,    0,  box_dimsz],
                            dimensions = 3,
                            position = particle_position,
                            type_name = particle_type,
                            mass = particle_mass,
                            diameter = particle_diameter,
                            moment_inertia = particle_inertia,
                            orientation = particle_orientation);
                            
system = hoomd.init.create_lattice(unitcell=uc, n=[1,1,1]);

system.particles.types.add('A');
system.particles.types.add('B');
system.particles.types.add('C');
system.particles.types.add('M');
system.particles.types.add('O');
snapshot = system.take_snapshot(all=True)
rigid = hoomd.md.constrain.rigid();
_=[]
p1=[]
p0=[]
for p in particles_list:
    p0.append((float(p[0]), float(p[1]), float(p[2])))
rigid.set_param('R0',
                types=['A']*lengthA +['B']*lengthB +['C']*lengthC,
                positions=p0);
rigid.set_param('N',
                types=['M']*2+['O']*(len(bead_position)-2),
                positions=bead_position);
rigid.create_bodies()
deprecated.dump.xml(group= group.all(), filename="unit_cell.xml",vis=True)

quit()
