import sys
import os
from hoomd import *
from hoomd import md
from hoomd import deprecated as hdepr
import numpy as np


coord = np.genfromtxt('cg_coord.txt', delimiter='\t')
charge = np.genfromtxt('charge.txt')
hand_A = [[2.2749, 2.599, 1.9701],[-1.1134, 3.2696, 1.9701],[-3.3883, 0.67062, 1.9701],
    [-2.2749, -2.599, 1.9701], [ 1.1134, -3.2696, 1.9701], [ 3.3883, -0.67062, 1.9701]]
z0 = 1.9701
sq3 = 1/2.0 * 3**0.5
coeff = [[1,0,0,-1],[-0.5, sq3, sq3, 0.5],[-0.5, -sq3, -sq3, 0.5],
[1,0,0,-1],[-0.5, sq3, sq3, 0.5],[-0.5, -sq3, -sq3, 0.5]]
hand_B = []
for i in range(6):
    hand_B.append(tuple([(coeff[i][0]*hand_A[i][0]+coeff[i][1]*hand_A[i][1]),
        (coeff[i][2]*hand_A[i][0]+coeff[i][3]*hand_A[i][1]), z0]))

l_mono = len(charge)

groupA=[]
mask=charge<-1.25
for i in range(l_mono):
    if mask[i]:
        for j in range(6):
            groupA.append(tuple(coord[i + j*30]))
groupA = groupA + hand_B

groupB=[]
mask1 = (charge>-1.25)
mask2 = (charge<-0.75)

for i in range(l_mono):
    if (mask1[i]*mask2[i]):
        for j in range(6):
            groupB.append(tuple(coord[i + 30*j]))

groupC=[]
mask1 = (charge>-0.75)
mask2 = (charge<-0.25)

for i in range(l_mono):
    if (mask1[i]*mask2[i]):
        for j in range(6):
            groupC.append(tuple(coord[i + 30*j]))

groupD=[]
mask1 = (charge>-0.25)
mask2 = (charge<0.25)

for i in range(l_mono):
    if (mask1[i]*mask2[i]):
        for j in range(6):
            groupD.append(tuple(coord[i + 30*j]))

groupE=[]
mask1 = (charge>0.25)
mask2 = (charge<0.75)

for i in range(l_mono):
    if (mask1[i]*mask2[i]):
        for j in range(6):
            groupE.append(tuple(coord[i + 30*j]))

groupF=[]
mask1 = (charge>0.75)
mask2 = (charge<1.25)

for i in range(l_mono):
    if (mask1[i]*mask2[i]):
        for j in range(6):
            groupF.append(tuple(coord[i + 30 * j]))

groupG=[]
mask = charge>1.25
for i in range(l_mono):
    if mask[i]:
        for j in range(6):
            groupG.append(tuple(coord[i + 30 * j]))

hexamer=groupA+groupB+groupC+groupD+groupE+groupF+groupG
len_A = len(groupA)
len_B = len(groupB)
len_C = len(groupC)
len_D = len(groupD)
len_E = len(groupE)
len_F = len(groupF)
len_G = len(groupG)
# build the body of molecle, hexamer is a list of tuples

l_body = len(hexamer)

#-------------------------HOOMD starts -------------------------------------------------
context.initialize()
# Place the type R central particles
uc = lattice.unitcell(N = 1,
                            a1 = [15, 0,   0],
                            a2 = [0,    15, 0],
                            a3 = [0,    0,   15],
                            dimensions = 3,
                            position = [[0,0,0]],
                            type_name = ['R'],
                            mass = [1.0],
                            moment_inertia = [[50,
                                               50,
                                               100]],
                            orientation = [[1, 0, 0, 0]]);
system = init.create_lattice(unitcell=uc, n=[1,1,1]);

# Add consituent particles of type A and create the rods
system.particles.types.add('A');
system.particles.types.add('B');
system.particles.types.add('C');
system.particles.types.add('D');
system.particles.types.add('E');
system.particles.types.add('F');
system.particles.types.add('G');
charges = {'A':-1.5, 'B':-1, 'C':-0.5,'D':0, 'E':0.5, 'F':1, 'G':1.5}

rigid = md.constrain.rigid();
rigid.set_param('R',
                types=['A']*len_A + ['B']*len_B + ['C']*len_C +['D']*len_D + ['E']*len_E + ['F']*len_F + ['G']*len_G,
                positions=hexamer);

#bodyGroup = group.type(name='body-particles', type='A')
rigid.create_bodies()

snap = system.take_snapshot(all=True)

################ testing block #############################
# position of the target particle : 2.2749, 2.599, 1.9701
print(snap.box)
print(snap.particles.N)
N_atom_hexamer = snap.particles.N
for i in range(181):
    print(str(i) + ' ' + str(snap.particles.position[i]))

snap.particles.resize(N_atom_hexamer + 6)
snap.bonds.resize(6)
print('testing output\n')
bond_len = 0.4

vec_bond = [[0.5*bond_len,0.866*bond_len,0], [-0.5*bond_len,0.866*bond_len,0], [-bond_len, 0,0],
[-0.5*bond_len,-0.866*bond_len], [0.5*bond_len, -0.866*bond_len,0], [bond_len,0,0]]
for idx in range(6):
    target = snap.particles.position[158+idx]
    snap.particles.position[N_atom_hexamer + idx] = [target[0]+vec_bond[idx][0], target[1]+vec_bond[idx][1], target[2]]
    snap.particles.typeid[N_atom_hexamer + idx] = 7
    snap.particles.charge[N_atom_hexamer + idx] = 1.0

    snap.bonds.group[idx] = [158+idx, N_atom_hexamer+idx]
    snap.bonds.types = ['bondA']
    snap.bonds.typeid[idx] = 0

system.restore_snapshot(snap)
system.replicate(nx=2, ny=1, nz=1)

p1 = system.particles[0]
p2 = system.particles[1]
print(p1.position)
print(p2.position[0])

print(system.particles)
for i in range(193):
    p = system.particles[i]
    x0 = p.position[0]
    y0 = p.position[1]
    z0 = p.position[2]
    p.position = (x0+5.0, y0, z0)
    print(p.position)
# Forcefield
harmonic = md.bond.harmonic(name="mybond")
harmonic.bond_coeff.set('bondA', k=330, r0=bond_len)
xmld = hdepr.dump.xml(group=group.all(), filename='config.xml',vis=True)
sigA = 1.0
nl = md.nlist.cell()
lj = md.pair.lj(r_cut=(sigA * 3.0), nlist=nl)
lj.pair_coeff.set(['A', 'B', 'C', 'D', 'E', 'F','G'], ['A', 'B', 'C', 'D','E','F','G'], epsilon=1.0, sigma=sigA)
lj.pair_coeff.set(['A', 'B', 'C', 'D','E','F','G','R'], 'R', epsilon=0, sigma=sigA)

kp=0.66
yuka = md.pair.yukawa(r_cut=3.0, nlist=nl)
types = ['A','B','C','D','E','F','G']
for type1 in types:
    for type2 in types:
        yuka.pair_coeff.set(type1, type2, epsilon=(5.0*charges[type1]*charges[type2]),kappa=kp)

yuka.pair_coeff.set(['A', 'B', 'C', 'D', 'E', 'F','G','R'], 'R', epsilon=0, kappa=kp)

group_rigid = group.rigid_center();
hands = group.charged()
#hands = group.tags(N_atom_hexamer, N_atom_hexamer+5)
group_integrate = group.union(name="tointegrate", a=group_rigid, b=hands)
ALL = group.all()
md.integrate.mode_standard(dt=0.0005)

integrator = md.integrate.nve(group=group_integrate, limit=0.01)
zero = md.update.zero_momentum(period=200)
run(1000)
integrator.disable()
zero.disable()

#integrator = md.integrate.npt(group=group_integrate, tau=1.0, kT=0.65, tauP=1.0, P=0.02)
dumper1 = dump.gsd(filename='initial.gsd',
         overwrite=True, period=50,
         group=group.all())
#run(2e4)
#integrator.disable()
#md.integrate.nve(group=rigid)
langv = md.integrate.langevin(group=group_integrate, kT=1.0, seed=42);
run(5e4)
dumper1.disable()

dumper2 = dump.gsd(filename='traj.gsd',
         overwrite=True, period=500,
         group=group.all())
run(5e5)

langv.disable()
#langv=md.integrate.langevin(group=rigid, kT=0.8, seed=47);
#run(2e5)

'''
DCD_dumpers = dump.dcd(filename = 'traj.dcd', period = 200, overwrite = True)

xmld = hdepr.dump.xml(group=group.all(), filename='final.xml')
'''
