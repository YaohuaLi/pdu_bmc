data=importdata('cg_3ngk.pdb');
cg_xyz=data.Model.Atom;
xyz=[cg_xyz.X; cg_xyz.Y; cg_xyz.Z];
scatter3(xyz(1,:),xyz(2,:),xyz(3,:))
axis equal
hold on
pi=3.141592653;
th=pi/3;
Rz=[cos(th),-sin(th),0;sin(th),cos(th),0;0,0,1];
rot1=Rz*xyz;
th=pi/3*2;
Rz=[cos(th),-sin(th),0;sin(th),cos(th),0;0,0,1];
rot2=Rz*xyz;
th=pi;
Rz=[cos(th),-sin(th),0;sin(th),cos(th),0;0,0,1];
rot3=Rz*xyz;
th=pi/3*4;
Rz=[cos(th),-sin(th),0;sin(th),cos(th),0;0,0,1];
rot4=Rz*xyz;
th=pi/3*5;
Rz=[cos(th),-sin(th),0;sin(th),cos(th),0;0,0,1];
rot5=Rz*xyz;

scatter3(rot1(1,:),rot1(2,:),rot1(3,:), 'filled')
scatter3(rot2(1,:),rot2(2,:),rot2(3,:), 'filled')
scatter3(rot3(1,:),rot3(2,:),rot3(3,:), 'filled')
scatter3(rot4(1,:),rot4(2,:),rot4(3,:), 'filled')
scatter3(rot5(1,:),rot5(2,:),rot5(3,:), 'filled')