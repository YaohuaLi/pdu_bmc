import sys
sys.path.append('/home/yrdahal/hoomd-install/')
sys.path.append('/home/yrdahal/hoomd-install/hoomd')
sys.path.append('/projects/b1030/hoomd/hoomd-2.2.0_cuda7.5/')
sys.path.append('/projects/b1030/hoomd/hoomd-2.2.0_cuda7.5/hoomd/')
#sys.path.append('home/yrdahal/hoomd-test/lib/python')
#sys.path.append('/home/yrdahal/hoomd-test/lib/python')
#sys.path.append('/projects/b1030/hoomd/hoomd-2.2.0_cuda8.0/')
#sys.path.append('/projects/b1030/hoomd/hoomd-2.2.0_cuda8.0/hoomd/')
import os
import random
import hoomd
#os.environ['LD_LIBRARY_PATH']  = "/home/jaimemillan/boost/lib" 
#print os.environ['LD_LIBRARY_PATH']

from hoomd import *
from hoomd.md import *
import hoomd.deprecated as deprecated 
import numpy as np 
c=context.initialize()

#buffer = option.get_user()
prot_r = 62.37
d_bb = 3.0
rad_M = d_bb/(2.0*2.0**(1./6))
rad_A = d_bb/(2.0*2.0**(1./6))
rad_N = 2.0
bead_N = 4
#l_l = 2.0*2.0**(1./6)*(rad_M + rad_N) + (bead_N - 3)*2.0**(1./6)*(rad_N + rad_N)
l_l = (bead_N -1)*1.12*4.0
l_l = bead_N**0.5 *4.0
req = l_l/(bead_N-1)
box_dims = 2.0/(3**0.5)*(2.0*prot_r+l_l+2.0*d_bb) #float(buffer[0])
#box_dims = 166.0 #float(buffer[0])
rad_B = 1.0#float(buffer[4])
rad_C = 3.0#float(buffer[5])
rad_R = 2.0 #float(buffer[6])
bead_position=[[2*rad_M,0,0],[-2*rad_M,0,0]]
#particles_list = np.genfromtxt("data_spherical_shell_noB.dat",dtype =str)
#particles_list = np.genfromtxt("data_nofake_particle.dat",dtype =str)
particles_list = np.genfromtxt("SBCG_ferritin_coordinate.dat",dtype =str)
total_ABC =len(particles_list)
lengthA=8
lengthB=0
lengthC=len(particles_list)-lengthA-lengthB
center_mass=65*lengthA+22*lengthB+50*lengthC
cage_diameter=25.0

uc = hoomd.lattice.unitcell(N = 2,
                            a1 = [box_dims, 0,   0],
                            a2 = [0,    box_dims, 0],
                            a3 = [0,    0,   box_dims],
                            dimensions = 3,
                            position = [[0,0,0],[0.5 * box_dims,0.5* box_dims,0.5 * box_dims]],
                            #position = [[0,0,0],[0.5 * 160,0.5* 160,0.5 * 160]],
                            #type_name = ['R0','R1','R2','R3','R4','R5','R6','R7','R8'],
                            type_name = ['R0','R0'],
                            mass = [center_mass,center_mass],
                            diameter = [2.0*cage_diameter,2.0*cage_diameter],
                            moment_inertia = [[200.0/3*center_mass*cage_diameter**2,
                                               200.0/3*center_mass*cage_diameter**2,
                                               200.0/3*center_mass*cage_diameter**2],[200.0/3*center_mass*cage_diameter**2,
                                               200.0/3*center_mass*cage_diameter**2,
                                               200.0/3*center_mass*cage_diameter**2]],
                            orientation = [[1, 0, 0, 0],[1, 0, 0, 0]]);
system = hoomd.init.create_lattice(unitcell=uc, n=[1,1,1]);
system.particles.types.add('A');
#system.particles.types.add('B');
system.particles.types.add('C');
snapshot = system.take_snapshot(all=True)
print snapshot.particles.N
#snapshot.particles.resize(total_ABC+2)
print snapshot.particles.types
print snapshot.particles.N
print total_ABC
print lengthB
#particles_listt =[[36.008,36.008,36.006],[36.008,36.008,-36.006],[36.008,-36.008,36.006],[-36.008,36.008,36.006],
#[-36.008,-36.008,36.006],[-36.008,36.008,-36.006],[36.008,-36.008,-36.006],[-36.008,-36.008,-36.006]];
#quit()
#snapshot = system.take_snapshot(all=True)
rigid = hoomd.md.constrain.rigid();
_=[]
p1=[]
p0=[]
for p in particles_list:
    p0.append((float(p[0]), float(p[1]), float(p[2])))
rigid.set_param('R0',
                types=['A']*lengthA +['B']*lengthB +['C']*lengthC,
                positions=p0);
rigid.create_bodies()
typeR = group.type(name ='typeR0', type ='R0')
typeA = group.type(name ='typeA', type ='A')
#typeB = group.type(name ='typeB', type ='B')
#typeAB = group.union(name ='typeAB', a= typeA, b = typeB)
#typeABR = group.union(name ='typeABR', a= typeAB, b = typeR)
typeC = group.type(name ='typeC', type ='C')
typeAC = group.union(name ='typeAC', a= typeA, b = typeC)
typeACR = group.union(name ='typeACR', a= typeAC, b = typeR)


#snapshot = system.take_snapshot(all=True)
print snapshot.particles.N
#quit()
print snapshot.particles.position[0]
print snapshot.particles.velocity[0]
print snapshot.particles.diameter[0]
print snapshot.particles.N

Lx =snapshot.box.Lx
Ly =snapshot.box.Ly
Lz =snapshot.box.Lz
print Lx,Ly,Lz

deprecated.dump.xml(group= group.all(), filename="single_bead.xml",vis=True)
#quit()
snapshot = system.take_snapshot(all=True)
initial_size = snapshot.particles.N
initial_bond_nbr = snapshot.bonds.N
#system.particles.types.add('M');
#system.particles.types.add('N');
#quit()
prot_r = 63.0
print prot_r
#quit()

N_A = 6.022 *(10) **23  # Avogadro's Constant reduced
molarity = 0.008 # in mole per litre
box_V = Lx *Ly *Lz #volume of a unit cell in Angstrom unit
prot_N = len(typeR)
vol_p = 4.0/3 * (np.pi * prot_r **3) #volume of a protein
vol_P = prot_N * vol_p #total volume of a protein in a unit cell
vol = (box_V - vol_P)*(10**(-30)) #volume available for polymers in m^3 unit
num_poly = int(vol * molarity/(10**(-3)) * N_A) # number of polymers in a unit cell. it is divided by 10^-3 to convert liter into m^3
nbr_poly =5*prot_N; #number of polymer
#nbr_poly =10; #number of polymer
print nbr_poly, num_poly
#quit()
monomer_types = ['M','N']
polymer_types = ['M-N'],['M-N'] #type of polymer
polymer_types_list = ['M'] + ['N']* (bead_N -2)+ ['M'] #list of beads type in a polymer
polymer_types_chain = ['M'] + ['N']* (bead_N -2)+ ['M']
diameters_hash_table = {'M':2.0*rad_M, 'N':2.0*rad_N} # diameter of of beads 
nbr_types_hash_table = {'poly':bead_N}
snapshot.particles.resize(initial_size + nbr_poly * bead_N)
snapshot.bonds.resize(initial_bond_nbr + nbr_poly * (bead_N-1))
snapshot.particles.types= snapshot.particles.types + monomer_types #initial type plus added type
true_idx= initial_size -1; 

print("***** Added Polymers ******")
print(" ")
print(" ")

monomer_added=[]
bond_group_list = []
angle_group_list = []
group_protein_center = group.type(name="a-particles", type= 'R0')
MCount = 0
VCount = 0
nbr_monomers_M = bead_N -1
#Add polymers
#First loop through chains
for i in range(nbr_poly):
 print("i: " + str(i))
 primer = (0.,0.,0.);
#Loop through monomer per chain
 for j in range(bead_N):
  print("j: " + str(j))
  true_idx= true_idx + 1 ; #particle numbers
  print("idx " + str(true_idx))

#change types and diameters
  #if true_idx < initial_size + nbr_poly * nbr_monomers_M:
  if j ==0 or j==(bead_N -1) :
            snapshot.particles.typeid[true_idx] = 3  
            snapshot.particles.diameter[true_idx] = 4.0
            snapshot.particles.mass[true_idx] = float(196/bead_N) #mass 
            snapshot.particles.velocity[true_idx] = 0.0001*(np.random.normal(size=3))
  else:
            snapshot.particles.typeid[true_idx] = 4 
            snapshot.particles.diameter[true_idx] = 4.0
            snapshot.particles.mass[true_idx] = float(196/bead_N)
            snapshot.particles.velocity[true_idx] = 0.0001*(np.random.normal(size=3))

	#add bonds

  nbr_mon_added =0; 

	#create new position
  Added = False;
	
  counter = 0;
  while(not Added):
	 #create random.random()om positions
   counter = counter + 1 ; 
   if j == 0:

    print("Adding primer ")
    x = Lx* (2.*random.random() -1)*0.5; 
    y = Ly* (2.*random.random() -1)*0.5; 
    z = Lz* (2.*random.random() -1)*0.5; 
    pos = (x,y,z)
    #print x, y, z
    #quit()
    rand_j0 = abs(2.*random.random() -1.)
   if j > 0:

    print("Adding monomer "  + str(j))
    x =  (2.*random.random() -1.) ; 
    y =  (2.*random.random() -1.) ; 
    z =  (2.*random.random() -1.) ; 
    
    curr_type = polymer_types_list[j]
    print curr_type
    print diameters_hash_table[curr_type]
    _p = monomer_added[-1]
    print _p
    _p_diameter = _p[3] #diameter of a bead
    norm = x**2.0 + y**2.0 + z**2.0;
    norm = norm**0.5 ;
    x /=norm;
    y /=norm;
    z /=norm;
    x *= 0.5*(diameters_hash_table[curr_type]  + _p_diameter) #right hand side is just the diameter of a bead
    y *= 0.5*(diameters_hash_table[curr_type]  + _p_diameter) 
    z *= 0.5*(diameters_hash_table[curr_type]  + _p_diameter) 
    x,y,z= x + primer[0],y+ primer[1],z+ primer[2]; 
    #print(primer,pos)
	
	 #check not inside protein
   
   INSIDE_PROT = False

   for id_prot,_p in enumerate(group_protein_center):
    print("id_prot : " + str(id_prot))
    print("pos_protein" + str(_p.position[0]) + " " + str(_p.position[1])+ " " + str(_p.position[2]))
    position = _p.position
    deltax = x - position[0]
    deltay = y - position[1]
    deltaz = z - position[2]
    r = (deltax**2.0 + deltay**2.0 + deltaz**2.0)**0.5;

    if r < (prot_r):
     print("xyz: "  + str(x)+ " " + str(y) + " " + str(z))
     print("r: "  + str(r))
     print("MaxProt : "  + str(prot_r))
     INSIDE_PROT = True ;

   print("INSIDE_PROT :" + str(INSIDE_PROT))
   OVERLAP = False
   if len(monomer_added) == 0:
    OVERLAP = False




   for _p in monomer_added:
	  #check if overlapping with other monomwes
    deltax = x - _p[0]
    deltay = y - _p[1]
    deltaz = z - _p[2]
    r = (deltax**2.0 + deltay**2.0 + deltaz**2.0);
    
    curr_type = polymer_types_list[j]
    _p_diameter = _p[3]
		
    if r  < 0.5* (diameters_hash_table[curr_type] + _p_diameter):
		 OVERLAP = True ;

      
   print("OVERLAP :" + str(OVERLAP))

   if not OVERLAP and not INSIDE_PROT:

    Added =True
    snapshot.particles.position[true_idx] = (x,y,z)
	  
    if j != bead_N -1:
     bond_group_list.append([int(true_idx),int(true_idx+1)])
    if j < bead_N -2:
     angle_group_list.append([int(true_idx),int(true_idx+1),int(true_idx+2)])
     #print bond_group_list
#    if j==2 :
 #    system.restore_snapshot(snapshot) 
		 #add to monomer list
    #print("ADDING MONOMER :" + str(j))
    curr_type = polymer_types_list[j]
    monomer_added.append([x,y,z, diameters_hash_table[curr_type]])
    #print monomer_added
    if j == 0:
  	 primer=(x,y,z)
  	 #print(primer,pos)
		 
    #print(primer,pos)  

   

    #print("ABORTING WHILE LOOP" + str(Added) + " " + str(j))

    print("ABORTING WHILE LOOP" + str(Added) + " " + str(j))

	#TODO
  #ADD PRIMER!!!
  #ADD MOnomer added!!!

#snapshot = system.take_snapshot()
snapshot.bonds.resize(nbr_poly*(bead_N-1))
snapshot.bonds.types=['M-N','N-N']
snapshot.angles.resize(nbr_poly*(bead_N-2))
snapshot.angles.types=['linear','right','obtuse']
#snapshot.bonds.types=['M-N']




for idx,b in  enumerate(bond_group_list):
 snapshot.bonds.group[idx] = [b[0],b[1]]
 #print [b[0],b[1]]
 if idx==0 or idx==bead_N-2 or (idx-(bead_N -2))%(bead_N -1)==0 or idx%(bead_N -1)==0: #first bond and last bond are of same type. (bead_N-2) refers to last bond of polymers. (bead_N-1) refers to fisrt bond of every other polymers except first one.
  snapshot.bonds.typeid[idx] = 0
 else:
  snapshot.bonds.typeid[idx] = 1
for idx,b in enumerate (angle_group_list):
 snapshot.angles.group[idx] = [b[0],b[1],b[2]]
 if idx==0 or idx%(bead_N -2)==0: #these are first angle
  snapshot.angles.typeid[idx] = 0
  #print snapshot.angles.typeid[idx]
 elif idx==(bead_N -3)==0 or (idx-(bead_N -3))%(bead_N -2)==0: #these are last angle
  snapshot.angles.typeid[idx] = 1
  #print snapshot.angles.typeid[idx]
 else: #these are middle angle
  snapshot.angles.typeid[idx] = 2
  #print snapshot.angles.typeid[idx]
system.restore_snapshot(snapshot) 
#system.replicate(nx=3,ny=3,nz=3)

deprecated.dump.xml(group=group.all(), filename="fer_cage_flexible.xml",vis=True)
#quit()
nl =nlist.cell()
harmonic = bond.harmonic(name="mybond")
harmonic.bond_coeff.set('M-N', k=338.0, r0=4.0)
harmonic.bond_coeff.set('N-N', k=338.0, r0=4.0)
#Mangle = angle.harmonic()
#Mangle.angle_coeff.set('linear', k=38.0, t0=np.pi) #first angle
#Mangle.angle_coeff.set('obtuse', k=38.0, t0=np.pi) #all middle angle
#Mangle.angle_coeff.set('right', k=38.0, t0=np.pi)#last angle
#nl =nlist.cell()
#torque= pair.cos_torque(r_cut = 5.0, nlist=nl)
#torque.pair_coeff.set(['R0','A','C','M','N','B'],['R0','A','C','M','N','B'] , epsilon=0.0, sigma=0.,r_cut= 0.0 )
#torque.pair_coeff.set('A','M' , epsilon=190.0, sigma=np.pi,r_cut=10)
sig=rad_M+rad_N
sigAB=rad_A+rad_B
sigAM=rad_A+rad_M
eps_small =1.0
eps_attract =1.0
lj= pair.lj(r_cut = sig*2**(1./6.), nlist=nl)
lj.set_params(mode="shift") 
def attract(a,b,sigma=1.0,epsilon=1.0,cutoff =1.0):
    #sigma = 'effective radius'
    lj.pair_coeff.set(a,b,epsilon=epsilon*1.0,
                      sigma=sigma*1.0 ,
                      r_cut = cutoff)

def repulse(a,b,sigma=1.0,epsilon = 1.0):
    #sigma = effective radius
    #r_cut = cutoff distance at the lowest potential (will be shifted to 0)

    lj.pair_coeff.set(a,b,epsilon=epsilon*1.0,
                      sigma=sigma,
                      r_cut=sigma*2.0**(1.0/6))
group_repulsive =['R0','A','C','M','N','B'];
for idx,_a in enumerate(group_repulsive):
    for _b in group_repulsive[idx:]:
        
	if _a and _b in group_repulsive:
	    rad_1 = rad_M;
            rad_2 = rad_A;
	    repulse(_a,_b, sigma= rad_1 + rad_2,epsilon=0.0)
            print _a, _b

group_repulsive =['A','C','M','N','B'];
for idx,_a in enumerate(group_repulsive):
    for _b in group_repulsive[idx:]:
        
	if _a and _b in group_repulsive:
	    rad_1 = rad_M;
            rad_2 = rad_N;
	    repulse(_a,_b, sigma= rad_1+rad_2,epsilon=1.0)
            print _a, _b
repulse('C','C',sigma= rad_C+rad_C,epsilon=1.0)
repulse('C','M',sigma= rad_C+rad_M,epsilon=1.0)
repulse('C','N',sigma= rad_C+rad_N,epsilon=1.0)
repulse('A','A',sigma= rad_A+rad_A,epsilon=1.0)
repulse('M','M',sigma= (rad_M+rad_M),epsilon=1.0)
#attract('A','M',sigma= rad_A +rad_M,cutoff=2.5*(rad_M+rad_A),epsilon=15.0)
attract('A','M',sigma= rad_M +rad_A,cutoff=2.5*(rad_M +rad_A),epsilon=20)
all = group.all();
typeR = group.type(name ='typeR0', type ='R0')
typeM = group.type(name ='typeM', type ='M')
typeN = group.type(name ='typeN', type ='N')
typeMN = group.union(name ='typeMN', a= typeM, b = typeN)
typeMNR = group.union(name ='typeMNR', a= typeR, b = typeMN)
integrate.mode_standard(dt=0.01)
#log Thermos
logger = analyze.log(quantities=['temperature' , 'pressure','potential_energy', 'kinetic_energy'],period=1e3, filename='test_flexible.log', overwrite=True)
#dcd file
traj = dump.dcd("traj_flexible.dcd",period=5e4,overwrite=True)
#nve integrator
integrator = integrate.nve(group=typeMN, limit = 0.01)
zero = update.zero_momentum(period =100)
run(1e3)
quit()
integrator.disable()
zero.disable()
#langevin integrator
integrator = integrate.langevin(group=typeMN, kT=1.0, seed=54260,dscale=0.08)
#integrator.set_params(kT=variant.linear_interp(points=[(0, logger.query('temperature')), (1e6, 0.9)]))
run(4e5)
deprecated.dump.xml(group= group.all(), filename="flexible_MN.xml",vis=True)
quit()
integrator.disable()
integrator = integrate.langevin(group=typeMNR, kT=0.95, seed=52660,dscale=0.08)
run(4e6)
deprecated.dump.xml(group= group.all(), filename="flexible_MNR095.xml",vis=True)
integrator.disable()
quit()
integrator = integrate.langevin(group=typeMNR, kT=1.0, seed=51660,dscale=0.08)
run(4e7)
deprecated.dump.xml(group= group.all(), filename="flexible_MNR1.xml",vis=True)
integrator.disable()
integrator = integrate.langevin(group=typeMNR, kT=1.02, seed=50660,dscale=0.08)
run(4e7)
deprecated.dump.xml(group= group.all(), filename="flexible_MNR102.xml",vis=True)
integrator.disable()
integrator = integrate.langevin(group=typeMNR, kT=1.04, seed=56660,dscale=0.08)
run(4e7)
deprecated.dump.xml(group= group.all(), filename="flexible_MNR104.xml",vis=True)
quit()
deprecated.dump.xml(group= group.all(), filename="flexible_MNR095.xml",vis=True)
integrator.disable()
integrator = integrate.langevin(group=typeMNR, kT=1.0, seed=53660,dscale=0.08)
run(3e7)
deprecated.dump.xml(group= group.all(), filename="flexible_MNR1.xml",vis=True)
integrator.disable()
integrator = integrate.langevin(group=typeMNR, kT=1.1, seed=53660,dscale=0.08)
run(3e7)
deprecated.dump.xml(group= group.all(), filename="flexible_MNR11.xml",vis=True)
quit()
integrator.disable()




integrator = integrate.nvt(group=typeMNR, kT=0.90,tau=0.95)
integrator.set_params(kT=variant.linear_interp(points=[(0, logger.query('temperature')), (1e9, 0.90)]))
run(3e7)
deprecated.dump.xml(group= group.all(), filename="manual_cage_MNR.xml",vis=True)
integrator.disable()
quit()
integrator = integrate.nvt(group=typeMNR, kT=0.94,tau=0.95)
#integrator.set_params(kT=variant.linear_interp(points=[(0, logger.query('temperature')), (1e6, 0.85)]))
run(2e7)
deprecated.dump.xml(group= group.all(), filename="manual_cage_nvtMNR.xml",vis=True)
quit()
integrator.set_params(kT=variant.linear_interp(points=[(0, logger.query('temperature')), (1e6, 0.9)]))

run(1e7)
deprecated.dump.xml(group= group.all(), filename="manual_cage_coolMR.xml",vis=True)
integrator.disable()

quit()



#npt integrator
npt_integrator = integrate.npt(group=typeR , kT=1.0 ,tau=0.65,P=0.000024,tauP=12.65,x=True,y=True,z=True,rescale_all=True)
npt_integrator.set_params(P=variant.linear_interp(points=[(0, logger.query('pressure')), (1.5e4, 0.000024)]))
deprecated.dump.xml(group= group.all(), filename="final2_npt.xml",vis=True)
run(3e5)
quit()



integrate.mode_standard(dt=0.08)
integrator = integrate.langevin(group=typeMNR, kT=0.9, seed=57100,dscale=0.02)
integrator.set_params(kT=variant.linear_interp(points=[(0, logger.query('temperature')), (1e6, 1.0)]))

run(7e6)
deprecated.dump.xml(group= group.all(), filename="final2_lan.xml",vis=True)
#quit()
#increase Temp
integrator.disable()

integrate.mode_standard(dt=0.08)
tf=1.0

integrator = integrate.nvt(group=typeMNR , tau = 0.65 , kT = 1.0)
integrator.set_params(kT=variant.linear_interp(points=[(0, logger.query('temperature')), (1e5, 0.75)]))

run(3e6)
#reduce Temp

integrator.set_params(kT=variant.linear_interp(points=[(0, logger.query('temperature')), (1.5e5, tf)]))
run(3e6)
deprecated.dump.xml(group= group.all(), filename="final2_nvt.xml",vis=True)
quit()
#NPT with Wall 
integrator.disable()
integrate.mode_standard(dt=0.1)
#print(walls.planes)
npt_integrator = integrate.npt(group=typeMNR , kT=tf ,tau=0.65,P=1e-2,tauP=0.65,all=True)
npt_integrator.set_params(P=variant.linear_interp(points=[(0, logger.query('pressure')), (1.5e7, 0.001)]))
deprecated.dump.xml(group= group.all(), filename="final2_npt.xml",vis=True)

run(3e5)
#wall_force_yukawa.wallobject_pull()
#print(walls.planes)


deprecated.dump.xml(group= group.all(), filename="final2.xml",vis=True)

